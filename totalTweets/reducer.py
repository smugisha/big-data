#!/usr/bin/env python

import sys

def reducer():
    totalTweets = 0
    for line in sys.stdin:
        data = line.strip().split('\t')
        if len(data) != 2:
            continue
        thisKey, thisValue = data
        totalTweets += int(thisValue)

    print('totalTweets\t{0}'.format(totalTweets))

if __name__ == "__main__":
    reducer()
