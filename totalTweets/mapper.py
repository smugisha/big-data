#!/usr/bin/env python

import sys
import json

def mapper():
    for line in sys.stdin:
        data = line.strip()
        if data != "":
            tweetData = json.loads(data)
            print( "{0}\t1".format(tweetData["text"].encode('utf-8')))

if __name__ == "__main__":
    mapper()
