#!/usr/bin/env python

import sys
import json

def mapper():
    index = 0
    for line in sys.stdin:
        data = line.strip()
        if data == "":
            continue

        tweetData = json.loads(data)
        tweet = tweetData['text'].encode('utf-8')
        index+=1
        print "{0}\t{1}".format(index, tweet)

if __name__ == "__main__":
    mapper()
