#!/usr/bin/env python

import sys
import json
from mm import *


def mapper():
    sentiment = SentiWordNetCorpusReader("SentiWordNet_3.0.0_20130122.txt")
    for line in sys.stdin:
        data = line.strip()
        if data == "":
            continue
        tweetData = json.loads(data)
        tweet = tweetData['text']
        a = wordnet_definitions(tag_tweet(tweet))
        obj_score = 0 # object score
        pos_score=0 # positive score
        neg_score=0 #negative score
        pos_score_tre=0
        neg_score_tre=0
        threshold = 0.75
        count = 0
        count_tre = 0

        """
        Conversion from plain text to SentiWordnet scores
        """
        
        for word in a:
            if 'punct' not in word :
                sense = word_sense_disambiguate(word['word'], wordnet_pos_code(word['pos']), tweet)
                if sense is not None:
                    sent = sentiment.senti_synset(sense.name)
                    # Extraction of the scores
                    if sent is not None and sent.obj_score <> 1:
                        obj_score = obj_score + float(sent.obj_score)
                        pos_score = pos_score + float(sent.pos_score)
                        neg_score = neg_score + float(sent.neg_score)
                        count=count+1
                        # print str(sent.pos_score)+ " - "+str(sent.neg_score)+ " - "+ str(sent.obj_score)+" - "+sent.synset.name
                        if sent.obj_score < threshold:
                            pos_score_tre = pos_score_tre + float(sent.pos_score)
                            neg_score_tre = neg_score_tre + float(sent.neg_score)
                            count_tre=count_tre+1

        #Evaluation by different methods
        
        avg_pos_score=0
        avg_neg_score=0
        avg_neg_score_tre=0
        avg_neg_score_tre=0
        
        #2
        
        if count <> 0:
            
            avg_pos_score=pos_score/count
            avg_neg_score=neg_score/count
        
        #3
        
        if count_tre <> 0:
            avg_pos_score_tre=pos_score_tre/count_tre
            avg_neg_score_tre=neg_score_tre/count_tre

        print "{0}\t1 {1}".format( timestamp,
                                 ("positivo" if pos_score > neg_score else
                                  "negativo" if pos_score < neg_score else
                                  "neutro") ) #tweetData['text'] )

if __name__ == "__main__":
    mapper()
