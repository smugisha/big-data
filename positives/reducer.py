#!/usr/bin/python
#!/usr/bin/env python

import sys

def reducer():
    totalCount = 0
    oldKey = None

    for line in sys.stdin:
        data = line.strip().split("\t")
        if len(data) != 2:
            continue
        thisKey, count = data

        if oldKey and oldKey != thisKey:
             print oldKey, "\t", totalCount
             oldKey = thisKey
             totalCount = 0

        oldKey = thisKey
        totalCount += int(count)

    if (oldKey != None):
        print ("{0}\t{1}".format(oldKey, totalCount))

if __name__ == "__main__":
    reducer()
