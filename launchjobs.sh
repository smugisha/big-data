#!/usr/bin/env bash

# store arguments in a special array 
args=("$@")

# get number of elements 
ELEMENTS=${#args[@]}

JOB_NAME=$1
MAPPER=$2
REDUCER=$3
INPUT=$4
OUTPUT_DIR=$5

for (( i=5;i<$ELEMENTS;i++)); do 
    MAPRED_FILES+=" -file ${args[${i}]}" 
done

$HADOOP_HOME/bin/hadoop jar $HADOOP_HOME/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.map.speculative=true \
    -D mapreduce.reduce.speculative=true \
    -D mapreduce.job.name=$JOB_NAME \
    -file $MAPPER -mapper $MAPPER \
    -file $REDUCER -reducer $REDUCER \
    -input $INPUT -output $OUTPUT_DIR \
    $MAPRED_FILES
