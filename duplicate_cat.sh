#!/usr/bin/env bash

file=$(basename $1)
fname="${file%.*}"
ext="${file##*.}"
x_times=$2

echo $file $x_times

mkdir duplicates


function numberOfDigits {
    n=$1
    ((a= $n<100000 ? $n<100 ? $n<10 ? 1 : 2 : $n<1000 ? 3 : $n<10000 ? 4 : 5 : $n<10000000 ? $n<1000000 ? 6 : 7 : $n<100000000 ? 8 : $n<1000000000? 9 : 10))
    echo $a
}

padding=$(numberOfDigits $x_times)

for (( i=1; i<=$x_times; i++ ))
do
    # fcode=$(printf "%.*d" $padding $i) 
    # new_fname="${fname}-${fcode}.${ext}"
    cat $1 >> duplicates/$file
done
