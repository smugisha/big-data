#!/usr/bin/env python

import sys

def reducer():
    totalTweets = 0
    oldKey = None

    for line in sys.stdin:
        data = line.strip().split("\t")
        if len(data) != 2:
            continue
        thisKey, thisCount = data

        if oldKey and oldKey != thisKey:
            print('{0}\t{1}'.format(oldKey, totalTweets))
            oldKey = thisKey
            totalTweets = 0

        oldKey = thisKey
        totalTweets += int(thisCount)

    if (oldKey != None):
        print ('{0}\t{1}'.format(oldKey, totalTweets))

if __name__ == "__main__":
    reducer()
