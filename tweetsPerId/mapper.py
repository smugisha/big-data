#!/usr/bin/env python

import csv
import sys
import json

def mapper():
    for line in sys.stdin:
        data = line.strip()
        if data != "":
            tweetData = json.loads(data)
            print('{0}\t1'.format(tweetData['user']['id']))

if __name__ == "__main__":
    mapper()
